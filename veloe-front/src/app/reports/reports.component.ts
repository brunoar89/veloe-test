import { Component, OnInit } from '@angular/core';
import { ReportService } from './../services/report.service';
import { Report } from '../models/report.model';

@Component({
  selector: 'reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  public report: Report;

  public report1: Array<any> = [];

  public report2: Array<any> = [];

  public report3: Array<any> = [];

  constructor(private reportService: ReportService) { }

  ngOnInit() {
    this.getReport();
  };

  getReport() {
    this.reportService.getReport().subscribe(
      (data) => {
        this.report = data;
        this.report1 = this.generateReport(data.dateVsTotal);
        this.report2 = this.generateReport(data.dateVsQuantity);
        this.report3 = this.generateReport(data.bankVsTotal);
      },
      (error) => {
        console.log("Error", error);
      }
    )
  };

  generateReport(data) {
    return Object
      .entries(data)
      .map(d => ({ name: d[0], value: d[1] }))
      .sort((a: any, b: any) => a.name < b.name ? -1 : 1);;
  };

  yAxisTickCurrencyFormatting = (value: number) => {
    let amount = new Intl.NumberFormat(
      "pt-BR",
      { style: "currency", currency: "BRL" }
    ).format(value);
    return amount;
  };

  yAxisTickNumberFormatting = (value: number) => {
    return value.toFixed();
  };

  formatMoney = (value) => {
    let amount = new Intl.NumberFormat(
      "pt-BR",
      { style: "currency", currency: "BRL" }
    ).format(value);
    return amount;
  }

}
