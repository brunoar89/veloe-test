export class Report {
  public entries: number;
  public remittances: number;
  public totalEntries: number;
  public dateVsTotal: any;
  public dateVsQuantity: any;
  public bankVsTotal: any;
}
