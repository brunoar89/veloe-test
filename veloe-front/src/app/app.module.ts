import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ReportsComponent } from './reports/reports.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';

import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ReportService } from './services/report.service';

const matComponents = [
  MatToolbarModule,
  MatGridListModule,
  MatCardModule,
];

@NgModule({
  declarations: [
    AppComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ...matComponents,
    NgxChartsModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  providers: [
    ReportService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
