import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Report } from '../models/report.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable()
export class ReportService {

  constructor(private http: HttpClient) { }

  public getReport():Observable<Report> {
    return this.http.get<Report>(`${environment.apiUrl}/reports`);
  }
}
