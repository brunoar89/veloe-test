const express = require("express");
const app = express();
const cors = require("cors");
const routes = require("./router");
const port = 9000;

app.use(cors());
app.use(routes);

app.get("/", (req, res) => {
  res.status(200).send("API STATUS: OK");
});

app.listen(port, () => {
  console.log(`API listening at http://localhost:${port}`);
});
