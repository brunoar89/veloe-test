module.exports = class ReportModel {
  constructor(path) {
    this.json = require(path);
  }

  getDateVsTotal() {
    try {
      const payload = this.json.listaControleLancamento.reduce((a, c) => {
        const currentAmount = a[c.dataEfetivaLancamento] || 0;
        a[c.dataEfetivaLancamento] = currentAmount + c.valorLancamentoRemessa;
        return a;
      }, {});
      return payload;
    } catch (error) {
      return {};
    }
  };

  getDateVsQuantity() {
    try {
      const payload = this.json.listaControleLancamento.reduce((a, c) => {
        const currentQuantity = a[c.dataEfetivaLancamento] || 0;
        a[c.dataEfetivaLancamento] = currentQuantity + 1;
        return a;
      }, {});
      return payload;
    } catch (error) {
      return {};
    }
  };

  getBankVsTotal() {
    try {
      const payload = this.json.listaControleLancamento.reduce((a, c) => {
        const currentAmount = a[c.nomeBanco.trim()] || 0;
        a[c.nomeBanco.trim()] = currentAmount + c.valorLancamentoRemessa;
        return a;
      }, {});
      return payload;
    } catch (error) {
      return {};
    }
  };

  getInfo() {
    try {
      const payload = {
        entries: this.json.totalControleLancamento.quantidadeLancamentos,
        remittances: this.json.totalControleLancamento.quantidadeRemessas,
        totalEntries: this.json.totalControleLancamento.valorLancamentos,
      }
      return payload;
    } catch (error) {
      return {};
    }
  };

  getReport() {
    try {
      const payload = {
        ...this.getInfo(),
        dateVsTotal: this.getDateVsTotal(),
        dateVsQuantity: this.getDateVsQuantity(),
        bankVsTotal: this.getBankVsTotal(),
      }
      return payload;
    } catch (error) {
      return {
        entries: 0,
        remittances: 0,
        totalEntries: 0,
        dateVsTotal: [],
        dateVsQuantity: [],
        bankVsTotal: [],
      }
    }
  }
}
