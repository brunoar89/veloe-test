const express = require("express");
const { get } = require("../controllers/reports");

const router = express.Router();

router.get("/", async (_, res, next) => {
  try {
    const response = await get();
    return res.json(response);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
