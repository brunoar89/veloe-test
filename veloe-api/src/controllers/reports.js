const ReportModel = require("../models/report");

exports.get = async () => {
  const report = new ReportModel("../data/lancamento-conta-legado.json");
  return report.getReport();
};