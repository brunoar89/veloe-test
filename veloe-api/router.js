const express = require("express");

const router = express.Router();

const reports = require("./src/routes/reports");

router.use("/reports", reports);

module.exports = router;
