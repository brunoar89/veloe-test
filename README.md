
# Veloe test
Sistema composto por uma API (veloe-api) e Front-end (velow-front) para analise de dados gráficos por usuários.
## Requirements
Node 12+
## API (veloe-api)
### Endpoints
Status
```console
[GET] /
```
Relatórios
```console
[GET] /reports
```
### Instalação (local)
1 - Clone o projeto
```console
git clone https://brunoar89@bitbucket.org/brunoar89/veloe-test.git
```
2 - Entre no diretório da aplicação
```console
cd veloe-test/veloe-api
```
3 - Instale os pacotes
```console
npm install
```
4 - Execute o comando para inicializar o projeto
```console
node server.js
```
Após concluir os passos a aplicação estará disponível no endereço: http://localhost:9000/
## Front-end (veloe-front)
### ENVS
É possível alterar os ENVs na seguinte pasta:
```console
veloe-test/veloe-front/src/environments
```
ENVs disponíveis:
- apiUrl
### Instalação (local)
1 - Entre no diretório da aplicação
```console
cd veloe-test/veloe-front
```
2 - Instale os pacotes
```console
npm install
```
3 - Execute o comando para inicializar o projeto
```console
ng serve --open
```
Após concluir os passos a aplicação estará disponível no endereço: http://localhost:4200/
